「姐姐，是那樣麼？」

不顧麻理子的危機， 亞里亞對米卡埃菈她們的話表現出了興趣。在比較了米卡埃菈她們二人之後，她又歪著腦袋抬頭看向麻理子。

「誒， 誒都 ……是怎麼樣呢？」

麻理子支支吾吾的，好不容易才忍住沒說出不要把話題往我這邊甩啊這樣的話。這個選項不管選了哪一方都是不明智的。被歸類於大的一方的麻理子要是說出「還是大的好」就會被說成「是自誇嗎」，而若是說出「還是小的好」則會有「是討厭嗎」這樣的反應。

麻理子向米蘭達透出了期盼的目光，而米蘭達僅僅只是在臉上露出複雜的神色保持著沉默。

「太大的話會妨礙到動作肩膀還會酸痛，經常一直被盯著那裡看，到底有什麼好的。珊醬不也經常這麼說的嗎」

「這個和那個是不一樣的，米卡醬才是，不是說過討厭再被遠遠的誤以為是男人了嘛。還說再稍微有一點會比較好」

「那種事才沒有說過！」

「說過的，絕對」

或許是因為麻理子她們沒有明確的表明態度，那兩人的矛頭開始對向了彼此。平時就一直再一起的話，也會知道對方會感到自卑的地方。將麻理子她們丟在一邊，自顧自的圍繞著大小展開了爭論。

「我很喜歡現在的這個尺寸所以感覺非常好！」

「我也是！」

在爭吵的過程中持續升級的那兩人，終於開始說出一些不明所以的話了。

「啊，兩位都……？」

「你們兩個都在吵些什麼！？」

就在麻理子看不下去打算插進兩人之間的時候，兩人的身後，同聲音一起在頭上降下了鐵拳。

「「好痛!?」」

「拘泥於自己的意見就不要把無關的人捲進來啊，你們幾個！」

「「卡桑……」」

米卡埃菈她們抱著頭回頭看去，雙手握著拳頭的卡琳正站在那裡。卡琳張開雙手就那樣伸出去，揪著兩人的耳朵。伴隨著那動作，這邊理所當然的也是裸著的卡琳的胸部開始晃動了起來。尺寸比起珊德拉要稍小了一點，可以認為是和旅館裡的伊莉同級別。

「等，卡桑!?　好痛的！」

「耳朵，不要扯了啊」

「 麻理子 ……桑，抱歉呢。這些姑娘們，好像稍微有點喝醉了」

卡琳一邊向麻理子告知著，一邊扯著二人的耳朵靈巧的向後退去。麻理子重新觀察了一下，被牽著的米卡埃菈她們的皮膚確實稍顯赤紅。想想看知道剛剛為止確實是好在食堂舉辦著歸還宴會。稍微有點醉了也並不奇怪。

「究竟是想給亞里亞醬灌輸些什麼！胸什麼的，大的小的不都沒關係嗎」

「那是因為對於托桑來說，不管是大是小都是卡桑才沒關係的啊」

「只要揉了就會變大，這麼說過的」

「等等，你們這是在說什麼!?」

「「痛痛痛！　要掉了！　耳朵要被扭斷了！」」

卡琳似乎也有一定醉了。能夠從回到了原來洗浴地方的那三人之間聽到這樣的對話。不由得和米蘭達面面相覷的麻理子 ，突然感到了個疑問。

「那個？　托桑是在指托爾斯頓先生的事嗎？　那麼他們兩位是……」

「啊， 麻理子殿是第一次見的緣故，所以不知道。那二人與其說是婚約者，不如說是大體上就是夫妻倆。據說巴爾特殿的小隊有著某種目標，只要達成了目標的話就會正式結婚」

「是那樣麼。確實感覺那兩位挺合得來的」

麻理子在頭腦中試著將托爾斯頓和卡琳並排放在一起 。身材高大的托爾斯頓和在女性裡個子算高的卡琳——比起一米六五的麻理子稍低一點——的組合，給人的感覺很好。

「因為和我們的工作有關係所以順便說一下。 巴爾特殿他們使用的有三個房間。三樓東南的角的房間是巴爾特殿 ，從那裡開始按順序是托爾斯頓殿和卡琳殿一間房，米卡埃菈殿和珊德拉殿一間房」

「房間也已經在一起了嗎」

「大家都是大人了，沒問題的」

就在麻理子感嘆著十九歲和二十歲就被當作大人對待的時候。回想了一下自己十九的時候也是這樣。麻理子就覺得理解了。

「吶，姐姐？」

「是？」

亞里亞的聲音，將深思的麻理子拉回了現實。

「所以，到底要怎樣做才能變的像姐姐一樣……」

（這麼說來，還在那個話題的途中啊！）

一邊看著向這邊仰視的亞里亞，麻理子一邊思考著應該如何回答這個問題。
===================
就是這麼回事，卡琳桑已經名花有主了。
如果錯字漏字等有，請指摘幸運。
