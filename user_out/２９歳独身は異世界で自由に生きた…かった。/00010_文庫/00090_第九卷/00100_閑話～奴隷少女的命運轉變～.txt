我是奴隷。是不如人類的存在，是活著的工具。我生來就一直被這樣教導著。
我的父母和兄弟姐妹也是奴隷。這是當然的，因為奴隷的孩子也是奴隷。我生活的世界和人類不同。再過不久，我就要離開這裡，被賣出去了。然後理所當然是被當成工具對待。
主人說過「你的長相和體格都很好，一定可以賣到好價錢」。現在只能打雜的我，努力讓自己能夠被以高價賣出，是我對現任主人應盡的義務和貢獻。
我也有向母親學習侍奉主人的方法，而且我還是尚未和男人發生過關係的處女。要被不曾謀面的新主人買下，讓我覺得有些害怕。因為母親說過，第一次會痛，要我忍耐一點。不僅如此，聽說有些主人還喜歡折磨奴隷，讓對方發出痛苦的哀號，真的好恐怖。
據說如果是被善良的主人買下，就能夠和人類一樣穿上乾淨的衣服、享用熱呼呼的飯菜，甚至是香甜的糕點。倘若能夠被那樣的主人買走，就太令人開心了。
我的買家好像已經確定了。看樣子，這筆交易似乎並非出自主人本意，所以主人顯得非常泄氣。不能繼續為現在的主人效忠，讓我有點傷心，因為是主人將我從小養育到現在的。

「因為是王室的命令，實在無可奈何⋯⋯你們自己好好保重。」

主人一臉泄氣，用憐憫的眼神望著我們。沒錯，是我們。這次被賣掉的不只是我，還有我的父母和弟妹們。雖然很可惜不能再為主人效忠，但我好高興可以和大家在一起，不用跟妹妹弟弟分開。
我們所有人坐上大馬車，出發啟程。我們要去的地方聽說叫做王都。聽車夫嘀咕說，抵達目的地一共要花上兩星期的時間，路途非常遙遠，但是好像可以不必用走的。居然可以坐馬車，簡直就像人類一樣。
從架在馬車上的車篷縫隙向外窺視，我發現有好幾名身穿閃亮盔甲的強悍騎士大人與我們同行。我曾聽說，騎士大人是比主人還要偉大的人物，可是為了運送我們，居然需要那樣的騎士大人隨行，這一點讓我覺得有些奇怪。
旅行期間可以說驚奇不斷。我見到了許多不曾見過的景色，還吃了好幾次以前從沒吃過的熱呼呼的食物。我的父母和弟妹們也好驚訝。先不說弟妹們了，既然連我父母都覺得訝異，那麼這一定是很不得了的事情。
而且在旅行途中，又多了我們以外的其他奴隷。大家都跟我們一樣，外型和人類不同，其中也有外表和我們相似的奴隷。我不禁心想，人類和我們奴隷果然是不同的生物。


─☆★☆─

「據說買下我們的是大樹海的魔王。」

昨天搭上馬車的男奴隷這麼說。他是立場和普通奴隷有些不同的知識奴隷，聽說他的職責是輔佐主人工作。他身上的服裝確實氣派得無法和我們普通奴隷相比，而且也有個人物品的行李。

「你說大樹海的魔王，這究竟是⋯⋯？」

父親這麼詢問男人。山羊獸人的知識奴隷閉上眼睛，搖搖頭。

「詳情我也不清楚。只不過主人說，那個魔王是曾經單槍匹馬二度襲擊王宮，將眾多騎士、士兵和魔法師全數抹殺的恐怖人物。前陣子，領地遭攻擊的魔王發狂暴怒，襲擊了王宮⋯⋯提出交出我們作為活祭品的要求。」

奴隷們聽了之後，不禁發出害怕的驚呼聲。我也好害怕。我緊抱怕到放聲哭泣的弟妹們。因為我是姐姐，所以就算害怕也不能哭。聽完這番話，馬車內原本有些歡樂的氣氛頓時冷了下來。
回頭想想確實奇怪。將一家人都買下來，給我們熱呼呼的食物，還讓我們坐馬車不必走路，這種事情實在太過美好了。
把熱呼呼的食物給我們這種身為工具的人，還用馬車載我們、讓我們不必走路，是為了不讓奴隷的品質在交給魔王之前下降。騎士大人們會同行，也不是為了護衛，而是要防止察覺自己即將被當成活祭品獻給魔王的我們逃跑。
我們嚇得發抖，彼此依偎著繼續隨馬車搖晃。見到我們那副模樣，車夫和騎士大人們露出不解的表情。這有什麼好奇怪的呢？我們奴隷也怕死，就只是這樣而已。
終於，我們抵達了王都。雖然一路上就只是坐著而已。
可是，即使只是坐著，不安和焦躁仍靜靜地折磨著我們。我是不要緊，但弟妹們的身體有些不適。見到那樣的我們，好像很偉大的人物責罵了車夫和同行的騎士大人們。

「為什麼這些奴隷的眼神這麼膽怯？甚至還有人的身體不舒服。」
「這我們也不清楚。他們明明直到中途，都還一副很開心的樣子。」
「你們該不會有虐待他們吧？」
「怎麼可能呢，我們才不敢做那麼恐怖的事情⋯⋯不然，就請你詢問奴隷們好了，因為我們也不知道原因，心裡覺得很困惑。」

我想起來了，自從我們搭乘的馬車的氣氛改變之後，就時常看見車夫和騎士大人面色凝重地交談，而且還會給我們香甜的糕點吃。心想這是可怜我們的騎士大人們，想在最後讓我們留下美好的回憶，我們不由得為其慈悲心腸流下眼淚。但奇怪的是，給我們糕點的騎士大人不知為何抱著頭，一副苦惱的模樣。
就這麼旁觀善良的騎士大人們遭到責備，實在忘恩負義。反正是即將成為活祭品之人，就算在此因做出無禮之舉而喪命，應該也沒什麼差。

「那、那個！」
「嗯？什麼事？」
「騎士大人們對我們非常慈悲。他們不但可怜即將被當成活祭品獻給魔王的我們，甚至還給我們熱食和香甜的糕點吃。因此，請您不要責怪他們。我們對騎士大人們只有感謝，完全沒有一絲怨恨。」

我將額頭貼在地面，以跪地叩首的姿勢對偉人說道。騎士比主人還要偉大，而面前的偉人比騎士更加偉大，像我這樣的奴隷直接朝偉人說話，是極為無禮的舉動。想必我一定會遭到斬殺，或是受到嚴厲的懲罰吧。儘管如此，只要我的話能夠幫助到騎士大人們⋯⋯

「魔王的活祭品⋯⋯？原來如此。」

偉人聽了我的話後陷入沉思。

「我明白你的話了，也知道他們並沒有錯。謝謝你告訴我。」
「是、是的。」
「但是，你們的認知是錯誤的。要求接收你們的人確實擁有強大的力量，可是他並非魔王，而是稀世的勇者大人。而且，勇者大人也是以像你們這樣的亞人為主要国民的勇魔聯邦之王。他恐怕是想將你們從奴隷的身份解放，讓你們以該国的国民身份活下去吧。」

我無法理解偉人所說的話。不是魔王而是勇者大人？我們即將不再是奴隷？不是奴隷之後，我們究竟會變成什麼呢？所謂的国民，到底該做什麼才好？我實在是想不透。
雖然想不透，但和我一起旅行的奴隷中，有好幾個人的表情變開朗了。如此說來，這應該是一件好事吧？我也應該覺得高興嗎？

「總之，我明白情況了。你們就洗去旅途的塵埃，好好休息吧。過幾天，你們會被引渡給對方。在那之前，你們就先好好地調養身體。」

偉人說完就離開了。騎士大人們露出鬆了口氣的表情，溫柔地把跪地叩首的我扶起來。

「你幫了我們大忙呢，小姑娘。」
「你要在那邊好好地生活喔。」

語畢，騎士大人們也離開，我們則在士兵們的引領下，來到一棟巨大的建築。我們好像要在這裡休息數日，之後才前往勇者大人的国家。
巨大建築裡，除了我們以外還有許多奴隷。聽說是從全国各地將我們這樣的亞人奴隷聚集起來。

「璐米娜！」

聽到有人喊我的名字，我回頭一望，結果看見大約一年半前被賣掉、與我分隔兩地的姐姐。姐姐朝我跑過來，緊抱住我。我也抱住姐姐。沒想到還能再見到姐姐，感覺簡直像在作夢一樣。

「父親、母親，還有立可、尼爾、涅姆也都在。真沒想到我能夠活著見到你們⋯⋯」

除了我之外，也見到其他家人的姐姐流下淚來。定睛一瞧，姐姐的身上滿是傷痕。買走姐姐的主人似乎對姐姐不太好。

「莉琳姐也在這裡，我帶你們去找她。」
「璐伊莉姐，你的身體⋯⋯」
「放心，已經不會痛了。因為魔法師大人幫我治好了。」

雖然璐伊莉姐這麼說，可是她滿身的傷痕讓人看了好心疼。就連姐姐從前最自豪的漂亮黑色尾巴也受傷脫毛了。

「你放心，沒事的。聽說我們即將要去的勇者大人的国家是個很棒的地方喔。大家都是這麼說的。」

走在前頭的璐伊莉姐的話，聽起來總覺得像是在說服自己似的。
後來，我們在那棟巨大建築裡生活幾天。我們不是用冰冷的水，而是用溫熱的水擦拭身體；吃的不是又硬又酸、快要發霉的麵包，而是剛烤好的柔軟麵包。不僅如此，餐點中甚至還出現了大塊的肉。若是從前，就連舉辦一年一度的收獲慶典時，都未必能夠吃到一小片肉。
偉人說我們不是魔王的活祭品，這是真的嗎？我們難道不是為了被當成活祭品獻給魔王，才被迫攝取營養嗎？我的心中產生這樣的懷疑。
可是儘管懷疑，我卻什麼也做不了。因為，我終究只是一個奴隷，只是會呼吸的工具罷了。即使被當成活祭品，也逃離不了那樣的命運。只不過，如果可以，我希望到時不會痛。
我們被引渡的時刻終於到了。被指示離開生活了好幾天的巨大建築後，我們以家族為單位聚在一起，等待那個時刻到來。
奴隷們的表情各不相同。有像璐伊莉姐一樣表情充滿希望的奴隷，也有像我一樣神情不安的奴隷。也有些奴隷的表情沒有什麼變化，我想他們一定是堅信不管身在何處，自己都還是奴隷。雖然我也覺得身為奴隷，好像應該跟他們一樣才對，但我就是做不到。

「總共三百八十七名嗎⋯⋯比想像中來得少耶。」
「因為亞人奴隷比較貴呀。再說，這還只是第一批。」
「說得也是，就只有這樣啊。」

正當我漫不經心地聽著面熟的偉人這麼說時，忽然一片嘩然。往前方望去，只見那裡有一名身穿可怕黑色盔甲的男人，還有幾名跟在那名男人身旁的亞人女性。熊獸人、狐獸人、精靈⋯⋯那是什麼種族呢？其中也有下半身是大蜘蛛的美麗女子。
偉人朝身穿黑色盔甲的男人跑去，將手裡的紙張交給他。盔甲男子和偉人交談了兩三句後，便點頭把紙交給蜘蛛女子。

「接下來我會打開轉移門，你們排隊依序進入。」

盔甲男子說完，下個瞬間，我突然有種渾身毛髮倒豎的感覺。我不曉得這是怎麼一回事，只知道回神時，在我們前方已經出現一個既像洞穴又像門，不知道是什麼的漆黑物體。
盔甲男子進到裡面，隨即又馬上回來。

「可以通過，沒有問題。莉法娜，雪莉，麻煩你們帶頭。」
「好。」
「知道了。那麼各位，請跟在我後面進到這個黑色的東西裡。沒有什麼好怕的，所以請各位不要急、不要慌，依照順序慢慢來。」

說完，精靈女子和狐獸人少女便進入黑幕之中。排頭的奴隷也跟著進入黑幕裡。可能是害怕闖進來歷不明的東西吧，奴隷們的移動速度十分緩慢。
話說回來，那個身穿黑色盔甲的男人就是魔王──不對，是勇者大人嗎？仔細一瞧，他的盔甲雖然散發出威嚇感，神情卻感覺莫名地溫柔。
我怔愣地看著他的臉，過了一會兒終於輪到我們了。我抱著必死決心衝進黑幕。結果，我感覺到瞬間的飄浮感，而且不知不覺間，眼前出現一片和方才所在的王都截然不同的景象。我覺得自己好像在作夢一樣。
這裡是地上舖有石板的廣場，廣場中央用黑色石頭蓋了一個高到必須仰望的柱子。大量的水從柱子前端流下來，很像是噴水池。
因為會阻礙通行，於是我立刻從黑幕旁離開。這裡的空氣氣味完全不同。濃郁的森林氣息和水的氣味中，混雜著感覺很美味的味道。朝味道傳來的方向望去，那裡似乎正在分送著湯。

「各位～這裡有在分送美味的湯喔～還沒有喝的人請來這邊領取～」

美麗的精靈女子一邊說，一邊把湯分給大家。我們受到那股香氣吸引，不由自主地靠過去。
領到的湯裡有大量蔬菜和肉，非常美味。隨著體內溫暖起來，原本躁動的心也慢慢平靜下來。就在這時，大概是所有人都移動到這裡了吧，黑幕泄氣似的消失了。消失後，身穿黑色盔甲的男子帶著蜘蛛女子和熊獸人女子，站在那裡。

「好了，大家辛苦了。歡迎來到幸運草！我的名字是大志・三葉！是你們以後的国王！請多指教！」


─☆★☆─

就這樣，很快地原本是奴隷的我成了勇魔聯邦的国民。聽說以後我不必再服侍主人，能夠憑自己的意志選擇自己想做的工作，自由活下去。
突然被這樣告知，我覺得很困擾。因為我是一個只懂得侍奉主人的奴隷。他們說，我們可以暫時體驗各式各樣的工作，然後從中找出自己想從事的職業。我心裡感到很不安。
儘管不安，但是能夠做自己喜歡的工作這一點，仍令我感到有些興奮。我想嘗試下廚，想試著縫製漂亮的衣服，也想做做看糕點。
覺得困擾的同時，內心卻又充滿興奮與雀躍。這是我第一次有這種感覺。我該怎麼做才好？雖然不知道，而且滿懷不安，但我不是孤單一人。我身旁有父親、母親、姐姐們，還有弟妹們。原本分散四處的家人團聚在一起了。只要全家人在一起，無論何種困難或許都有辦法克服。嗯，一定沒問題。
我不用跟家人分離，肯定是托那個身穿黑色盔甲的男人的福。總之，先向那個人道謝吧。下定決心的我把湯碗還回去，然後朝黑色盔甲男子走去。