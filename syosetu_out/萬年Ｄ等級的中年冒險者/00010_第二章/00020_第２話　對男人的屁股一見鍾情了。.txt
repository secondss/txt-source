皇家聖德古拉騎士學院的宿舍門口處。

在這所學院上學的學生，大多數都是住在宿舍的。住在外邊的只有極少數。

因為是皇家開設的貴族學校，所以有大浴場和高級餐館。當然，普通的食堂之類的也是有的。

金錢方面沒有富裕的我們選擇了申請住宿，申請之後馬上就合格了。
然後就如同申請的一樣，從今天開始就要住進宿舍了。

「男生宿舍是在右邊嗎？」
「女子宿舍是這邊啊，回頭見呢。」

男生宿舍和女子宿舍稍微分開了一些位置。
我是艾莉婭分別後，一個人前往了男生宿舍。
不，真的是一個人嗎⋯⋯

「吾、吾輩想去女生宿舍吶！！！」
「你確實是女孩子⋯⋯但是因為你是我的劍啊，所以如果不在我身邊的話會很為難的。」
「嚯嚯，就是沒有吾輩就活不下去的意思了嗎？」
「用那副姿態的話各個方面都會引起誤解的所以快住手！」

幼女化的維納斯是和我一起的。

「等會一定要變回劍啊。我這種大叔帶著小孩子進宿舍入學什麼的，變成那種印象我可是非常討厭的啊。畢竟我都是這樣的年齡了。」
「呀嘞呀嘞、真是沒辦法呀～」

維納斯一臉不滿地變成了劍的姿態，回到了鞘中平靜了下來。

「嗯⋯⋯我的房間，是⋯⋯」

在寬廣的走廊前進著，我正尋找著被分配的房間。
從管理宿舍的阿姨那裡收到的是２１３‐Ｂ的鑰匙，房間是在二樓。

「哦，是這裡嗎？」

將刻著２１３大字的大門，推門而入。

引入眼簾的是、十五個榻榻米左右寬的寬敞的客廳。

毛地毯上面擺放著真皮的沙發。
而且有些疑似是高等傢俱的東西。

身為平民的我看到這樣的房間反而變得忐忑不安了起來。

但是，這裡是共用的房間。

兩人一組的房間、有著一個客廳和廁所，還有浴室──大浴場，這些是每個房間都附有的──而且都是相同的構造。

有Ａ、Ｂ、兩個單間，我是Ｂ的那個人⋯⋯

「我的房間到底是哪個門？」

有很多個門，所以不知道哪一個是前往Ｂ房間的。
所以我先隨意地打開了附近的門。
然後在下一瞬間，就為那個行為感到後悔了。

「啊⋯⋯」

運氣不好，有人在裡面。

而且還是一絲不掛的樣子。

水滴從光澤的黑髮上面滴落下來，落在了被曬得健康了的皮膚上。
那副模樣，大概是在洗完澡後不久吧，因為他（她）正準備拿掛在牆壁上的浴巾。

「呀！？」

回頭注意到我後，被嚇得全身都打了個寒戰。

「哇，對不起！」

我慌慌張張地道歉後，馬上就把門關上了。

這裡好像是一直通往浴室的更衣室。
然後因為是在用這裡的設施，所以那個人一定是Ａ房間的在校生吧。

真是的，至少敲門吧，我。
不幸中的萬幸大概是他（她）是背向我這裡的嗎？

真是個可愛的孩子啊⋯⋯⋯

雖然只看到了臉，但年齡大概是在十幾歳左右吧。
是一個給人柔和印象的美少女。

但是，在我腦海中烙上的卻是「臀部」

那是一對圓潤的同時也給人感覺非常緊致、對稱上朝的屁股。

和稍微曬黑了的其他肌膚不同，只有那裡如同初雪一般白皙。
那麼美麗、那麼好的形狀的屁股，是我有生以來第一次看到，也說不定。

《想揉就去揉嘛，幹嘛苦了自己！》

雖然想不承認那傢伙說的話，但也不能否定。

啊，等一下啊？

這裡是男生宿舍吧？
這麼說的話，那、剛才的那個是⋯⋯男人？

這麼說起來，我總覺得那張臉在好像在哪裡見過，就是在入學考試的時候。
他就是向對於陷入困境的我們詢問，要不要棄權的少年。

我的理智崩塌了。

「我現在難道看上了男人、的屁股了嗎⋯⋯？」

我兩手貼在地板上頭也跟著下垂的時候，從背後傳來了聲音。

「誒⋯⋯？」

門稍微開了一點，剛才的美少女──不，美少年露出了一半的臉。

也許是因為是剛剛洗澡的緣故吧，他的臉頰很紅。

為了使害羞消散而咳嗽了一聲，然後這麼說道。

「你就是從今天開始使用Ｂ房間的新生，是吧⋯⋯？」
「啊，是。還有剛才對不起了。」
「不，沒事！沒關係！我沒能鎖好門也是我的不好⋯⋯不、不好。」

一邊這樣說著一邊從更衣室裡面完全出來了。
那副身子對於男人來說有點小了。
有著長長的睫毛，可愛小巧的嘴唇，即使被認成是女孩子也沒有辦法的事啊。
但是那樣的他卻穿著男性的衣服。

「請不要敬語。你是前輩，而我則是後輩。」
「啊，是啊⋯⋯是嗎？⋯⋯嗯⋯⋯」

雖然我比你大幾十歳⋯⋯