１９

「知道〈深度〉這東西嗎？」
「知道。但是，以為只是表示迷宮品的等級或是格的數值」
「呼嗯」
「雷肯殿是對什麼感到了驚訝嗎？」
「沒感到驚訝」
「那麼是在擔心什麼嗎？」
「沒在擔心」
「呼─嗯」
「去九十階層吧」
「今天不做休養日嗎」
「昨天充分休息了吧」

昨天是跟〈遊手好閒〉的共同探索。至今以兩人對付十隻魔獸，變成以五人對付就減輕了很多負擔。

但今天要兩人去九十階層。九十階層會出現十隻敵人。阿利歐斯會有意見也是當然的。
然而實際進了九十階層後，不可思議地輕鬆地打倒了十隻敵人。也出了恩寵劍。

「嗯─。果然大型個體和普通個體的手感相當不同呢」
「這雖然也是原因，但探索到九十四階層的影響也很大吧」
「迷宮真是可怕的地方呢。另外，雷肯殿的那把劍也很棒呢」
「不會給你喔」

那把劍指的是〈奧多之劍〉。上次和九十階層的大型個體戰鬥時，使用了〈拉斯庫之劍〉。現在是抱著會壊掉的覺悟在使用〈奧多之劍〉。

與鑑定師老人的對話，讓雷肯注意到了這把劍在這世界上具有的特殊價值。然後，在別人面前使用這武器應該不太妙。
但是，在迷宮戰鬥的途中，心情冷靜下來了。

仔細一想，這把劍具有的機能，〈鋒利度附加〉、〈威力附加〉和〈重量附加〉，這世界的恩寵劍也有。三個都有的雖然可能很稀奇，但劍本身的性能，並沒有多破格。

也就是說，就算在使用時被看到了，也不會覺得異常。
要是被施了〈鑑定〉，就算知道是恩寵品，也不會知道詳細的部分。就算知道，也不會知道那是來自賦予師的追加恩寵。

這麼一想後，就沒有任何必要在使用〈奧多之劍〉上顧慮了。
還有注意到另一件事。那就是〈阿格斯特之劍〉。

與尼納耶迷宮之主戰鬥時，雷肯解放了最大限度的〈澤娜的守護石〉的守護，以全力的斬擊攻擊了女王蜘蛛。
然而，〈阿格斯特之劍〉卻沒有折斷。完全沒有。那是普通的劍的話會無法承受並折斷也不可思議的威力。
〈阿格斯特之劍〉的堅固性高得異常。

這應該要早點注意到的。
順帶一提，就算試著對雷肯持有的幾把劍施〈鑑定〉，也讀取不了〈深度〉。那老人確實說了，〈鑑定〉進步到上級的話就能讀取〈深度〉。現在的雷肯還讀取不了〈深度〉。

「好。去下個房間吧」
「是。已經隨您便也無所謂了」

結果，在這天制霸了九十階層的十間房間。移動距離短的話探索就輕鬆了。恩寵品總共出了四把。沒有恩寵的武器也出了三把。也出了青紫藥水和紅紫藥水。

隔天，三十二日，與〈遊手好閒〉的共同探索前進到了九十六階層。出了三個恩寵武器，賣了大金幣九枚。雖然環顧了收購所，但沒看到叫特雷門的老鑑定士。

三十三日也進行了共同探索。在兩間房間與普通個體戰鬥後挑戰了大型個體，前進到了九十七階層。
三十四日是做休養日，便和阿利歐斯走遍了十五間九十階層的普通個體的房間。這天是藥水類的中獎日。也出了一把恩寵劍。
三十五日以共同探索前進到了九十八階層。出了一把恩寵劍，賣了大金幣八枚。雷肯把販賣交給布魯斯卡，自己沒進入收購所。
三十六日也進行了共同探索，前進到了九十九階層。雷肯雖然說要直接打倒九十九階層的大型個體前往百階層，但被另外四人猛烈反對。那麼就明天挑戰吧，雷肯雖然這麼說，但四人也沒有聽從。

「雷肯。所謂百階層，是特別的」
「所以才想早點去不是嗎」
「搞不懂你的感覺。總之有必要做好萬全的準備」
「是阿。連日的猛進擊應該讓疲勞累積了。雖然狀態不錯感覺不到就是了」
「感覺不到的話就不需要在意」
「前陣子就想過了，你阿，有點怪怪的吶。所以才能辦到那種離譜的攻略吧」
「雷肯殿。到三十九日之前都做休養日吧」
「休息那麼多會讓身體變遲鈍喔」
「在四十日挑戰九十九階層的大型個體。可以吧」

結果，在四人的逼迫下，定了三天的休養日。

然後，在休養日結束後的一月四十日。
〈虹石〉和〈遊手好閒〉，平安突破了九十九階層。

走下前往百階層的階梯，腳步很輕盈。
雖然是寬廣的階梯，但沒看到有其他人也在走。

雖然沒有交談，但〈遊手好閒〉三人的興奮感傳遞了過來。
不久後，走完了階梯。
走完長階梯，踏入百階層時，一行人發出了感嘆之聲。

那階層的模樣，和九十九階層之前，完全不一樣。
延伸著岩石迴廊這點一樣。但是，顏色不同。
不論是地板，牆壁還是天花板，所有的岩石都散發著淡藍色的光輝。
被朦朧的青白光芒照耀，五人的身姿染上了青色。

「⋯好漂亮」
「宛如不同的世界吶」
「這就是，百階層」
「幻想般的光景呢」

雷肯因為過度驚訝，凍結了。

〈生命感知〉是，感知直徑約兩千四百步的範圍內的生命體的能力。
現在，雷肯的〈生命感知〉，映照出了〈虹石〉和〈遊手好閒〉的五人。
至於其他，沒有映照出任何一種光點。

「〈圖化〉！」

發動了〈圖化〉。這是表示迷宮裡的所在階層的所有構造，還會表示那一階層裡存在的魔獸的魔法。

根據這個，這階層有九間房間。一想到至今的九十九個階層都有兩百甚至更多的房間，這落差讓人只能感到驚訝。不過，至今的房間雖然都很小，但這百階層的房間非常大。不過依然只有九間房間，所以不同於之前的階層，雷肯的〈生命感知〉能一眼暸望階層全體。

但是，沒有。
沒有魔獸。

不論是〈生命感知〉還是〈圖化〉，一隻魔獸都沒有映照出來。

百階層，是沒有魔獸存在的階層。